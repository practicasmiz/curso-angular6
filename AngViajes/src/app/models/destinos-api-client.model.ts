import { DestinoViaje } from './destino-viaje.model';


export class DestinosApiClient {
    [x: string]: any;
    destinos: DestinoViaje[] = [];

    constructor() { 
        this.destinos = [];
    } 


    add(d: DestinoViaje) {
        this.destinos.push(d);
    }

    getAll(): DestinoViaje[] {
        return this.destinos;
    }
  
    elegir(d: DestinoViaje) {
        return this.destinos;
      }

 }

 