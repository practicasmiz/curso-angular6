import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ReactiveFormsModule, FormBuilder, FormGroup } from '@angular/forms';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.scss']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;

  constructor(fb: FormBuilder) {
    this.onItemAdded=new EventEmitter();
    this.fg=fb.group({
      nombre:[''],
      url: ['']
    });

    this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio el formulario: ', form);
    });
   }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string):boolean {
    let d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 5) {
      return {invalidNombre: true};
    }
      return {invalidNombre: false};
  }
  

    nombreValidatorParametrizable(minLong: number): ValidatorFn {
      return (control: AbstractControl): { [s: string]: boolean} | null => {
        const l = control.value.toString().trim().length;
        if (l > 0 && l < this.minLongitud) {
          return {minLongNombre: true};
        }
          return {minLongNombre: false};
      }
    }
      
  
}
